#!/bin/bash

mongo=( mongo --host 127.0.0.1 --port 27017 --quiet )

for file in /docker-entrypoint-initdb.d/dump/*; do
    case "$file" in
        *.js)
            serviceName=$(basename "${file%.*}")
            echo "$0: initializing database $serviceName"

            "${mongo[@]}" "$serviceName" <<-EOJS
                db.createUser({
                    user: $(_js_escape "$serviceName"),
                    pwd: $(_js_escape "$serviceName"),
                    roles: [ { role: 'dbOwner', db: $(_js_escape "$serviceName") } ]
                })
EOJS

            "${mongo[@]}" "$serviceName" "$file" ;;
        *)
            echo "$0: ignoring $file" ;;
    esac
done

