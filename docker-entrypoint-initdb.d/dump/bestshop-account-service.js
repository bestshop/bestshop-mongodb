
print('Start to dump account data');

db.accounts.insert(
    {
    "_id": "garyzhang",
        "_class": "io.gary.bestshop.account.domain.Account",
        "email": "gary.zhang@test.com",
        "password": "password",
        "nickname": "Gary Rocks",
        "firstName": "Gary",
        "lastName": "Rocks",
        "birthDate": ISODate("1987-12-07T23:00:00.000Z"),
        "mobilePhone": "63212353",
        "purchaseCount": 0,
        "purchaseAmount": "0.00",
        "status": "Enabled",
        "createdAt": new Date(),
        "lastModifiedAt": new Date()
    }
);

print('Finish to dump account data');