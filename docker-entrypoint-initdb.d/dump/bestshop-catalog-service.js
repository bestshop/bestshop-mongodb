
print('Start to dump catalog data');

db.products.insert(
    {
            "_class" : "io.gary.bestshop.catalog.domain.Product",
            "name" : "Powerful Lessons in Personal Change",
            "brand" : "Kindle Edition",
            "category" : "BooksAudible",
            "description" : "What are the habits of successful people? The 7 Habits of Highly Effective People has captivated readers for 25 years. It has transformed the lives of Presidents and CEOs, educators, parents, and students — in short, millions of people of all ages and occupations have benefited from Dr. Covey's 7 Habits book. And, it can transform you.",
            "price" : "11.19",
            "purchaseCount" : 0,
            "createdAt" : ISODate("2018-03-31T21:48:06.453Z"),
            "lastModifiedAt" : ISODate("2018-03-31T21:48:06.453Z")
    }
);

print('Finish to dump catalog data');