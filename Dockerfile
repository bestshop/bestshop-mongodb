FROM mongo:3.7

ADD docker-entrypoint-initdb.d /docker-entrypoint-initdb.d
RUN chmod a+w  /docker-entrypoint-initdb.d/dump/*.js

ENV MONGO_INITDB_ROOT_USERNAME=root
ENV MONGO_INITDB_ROOT_PASSWORD=password
